# 编程题 - 使用 Grunt 完成项目的自动化构建 - 项目说明文档

1. 下载基础代码

2. 需求分析

   1. 文件类型包括 html、js、scss、字体文件、图片

   2. 自动化构建要完成：读取文件 - 加工文件（样式编译、脚本编译、页面模板编译、图片和字体文件转换、文件压缩） - 写入文件

   3. 开发服务器、监视变化

   4. 需要支持的命令：

      ```
      yarn lint
      yarn compile
      yarn serve
      yarn build
      yarn start
      yarn deploy
      ```

3. 将 gulpfile.js 删除，新建 gruntfile.js 文件

   1. 安装 grunt，并按需引入

      ```
      yarn add grunt --dev
      ```

   2. 安装并导入自动加载所有 grunt 插件中的插件

      ```bash
      yarn add load-grunt-tasks --dev
      ```

      ```javascript
      const loadGruntTasks = require('load-grunt-tasks')
      ```

   3. 样式编译

      ```
      yarn add grunt-sass sass --dev
      ```

      ```javascript
      module.exports = grunt => {
       grunt.initConfig({
         sass: {
           options: {
             implementation: sass
           },
           main: {
             files: [{
                 expand: true,
                 cwd: 'src/assets/styles',
                 src: ['*.scss'],
                 dest: 'dist/assets/styles',
                 ext: '.css'
             }]
           }
         }
       })
       ...
      }
      ```
      
   4. 脚本编译

      ```
      yarn add grunt-babel @babel/core @babel/preset-env --dev
      ```

      ```javascript
      babel: {
          options: {
             presets: ['@babel/preset-env']
          },
          main: {
              files: [{
                  expand: true,
                  cwd: 'src/assets/scripts',
                  src: ['*.js'],
                  dest: 'dist/assets/scripts',
                  ext: '.js'
              }]
          }
      }
      ```
      
   5. 页面模板编译

      ```
      yarn add grunt-web-swig --dev
      ```

      ```javascript
      web_swig: {
          options: {
              swigOptions:{
                  cache: false
              },
              getData: () => {
                return data
              }
          },
          main: {
              files: [
                  {
                      expand: true,
                      cwd: 'src/',
                      src: ['*.html'],
                      dest: 'temp/'
                  },
              ]
          }
      }
      ```

      

   6. 图片和字体文件转换

      ```
      yarn add grunt-contrib-imagemin --dev
      ```

      ```javascript
      imagemin: {
            static: {
              options: {
                optimizationLevel: 3,
                svgoPlugins: [{removeViewBox: false}]
              },
              files: [{
                expand: true,
                cwd: 'src/',
                src: ['**/*.{png,svg}'],
                dest: 'dist/'
              }]
            }
      }
      ```

      

   7. 文件清除

      ```
      yarn add grunt-contrib-clean --dev
      ```

      ```javascript
      clean: {
      	dist: 'dist/**',
          temp: 'temp/**'
      }
      ```

   8. useref 文件引用及文件压缩处理

      ```
      yarn add grunt-useref --dev
      ```

      ```javascript
      useref: {
          html: 'temp/*.html',
          temp: 'temp'
      }
      ```

      

   9. 压缩 js 文件

      ```
      yarn add grunt-contrib-uglify --dev
      ```

      ```
      uglify: {
      	main: {
              files: [{
                  expand: true,
                  cwd: 'temp/assets/scripts',
                  src: ['*.js'],
                  dest: 'dist/assets/scripts'
              }]
          }
      }
      ```

      

   10. 压缩 css 文件

       ```
       yarn add grunt-css --dev
       ```

       ```javascript
       cssmin: {
           main: {
               files: [{
                   expand: true,
                   cwd: 'temp/assets/styles',
                   src: ['*.css'],
                   dest: 'dist/assets/styles'
               }]
           }
       }
       ```

       

   11. 压缩 html 文件

       ```
       yarn add grunt-contrib-htmlmin --dev
       ```

       ```javascript
       htmlmin: {
             options: {
               removeComments: true,       // 去除注释信息
               collapseWhitespace: true,    // 去除空白字符
               removeEmptyAttributes: true,  // 去除标签的空属性
               removeCommentsFromCDATA: true, // 去除 CDATA 的注释信息
               removeRedundantAttributes: true     // 去除标签的冗余属性
             },
             build: {
               files: [{
                 expand: true,
                 cwd: 'temp',
                 src: ['*.html'],
                 dest: 'dist'
               }]
             }
       }
       ```

       

   12. 文件复制

       ```
       yarn add grunt-contrib-copy --dev
       ```

       ```javascript
       copy: {
           public: {
           	expand: true,
               src: 'public/**',
               dest: 'dist/',
           }
       }
       ```

       

   13. 校验

       ```bash
       yarn add grunt-contrib-jshint --dev
       ```

       ```javascript
       jshint : {
           options : {
               camelCase : true
           },
           core : {
               src : 'src/assets/scripts/*.js'
           }
       }
       ```

       

   14. 开启服务

       ```
       yarn add grunt-contrib-connect --dev
       ```

       ```javascript
       connect: {
           options: {
               port: 9000,
               hostname: '*', //默认就是这个值，可配置为本机某个 IP，localhost 或域名
               livereload: 35729  //声明给 watch 监听的端口
           },
       
           server: { // 开发
               options: {
                   open: true,
                   base: ['temp', 'src', 'public'],
                   routes: {
                   	'/node_modules': 'node_modules'
                   }
               }
           },
           build: { // 构建
               options: {
                   open: true,
                   base: ['dist']
               }
           },
       }
       ```

       

   15. 监视变化

       ```
       yarn add grunt-contrib-watch --dev
       ```

       ```javascript
       watch: {
             options: {
               livereload: '<%=connect.options.livereload%>'  //监听前面声明的端口  35729
             },
             js: {
               files: ['src/**/*.js'],
               tasks: ['babel']
             },
             css: {
               files: ['src/**/*.scss'],
               tasks: ['sass']
             },
             html: {
               files: ['src/*.html', 'src/**/*.html'],
               tasks: ['web_swig']
             }
       }
       ```

   16. 规划构建任务

       ```javascript
       grunt.registerTask('compile', ['babel', 'sass', 'web_swig'])
       grunt.registerTask('compress', ['uglify', 'cssmin', 'htmlmin'])
       
       grunt.registerTask('clear', ['clean'])
       grunt.registerTask('lint', ['jshint'])
       grunt.registerTask('build', ['clear', 'compile', 'useref', 'compress', 'imagemin', 'copy'])
       grunt.registerTask('serve', ['connect:server', 'watch'])
       grunt.registerTask('start', ['build', 'connect:build'])
       grunt.registerTask('deploy', ['compile', 'serve'])
       ```

       ```json
       "scripts": {
           "clean": "grunt clear",
           "lint": "grunt lint",
           "serve": "grunt serve",
           "build": "grunt build --force",
           "start": "grunt start --force",
           "deploy": "grunt deploy --production"
        }
       ```

       

4. 运行命令测试
