// 实现这个项目的构建任务
const sass = require('sass')

// 自动加载所有 grunt 插件中的插件
const loadGruntTasks = require('load-grunt-tasks')

// 开发服务器、监视变化
const browserSync = require('browser-sync')
const bs = browserSync.create()

const data = {
  menus: [
    {
      name: 'Home',
      icon: 'aperture',
      link: 'index.html'
    },
    {
      name: 'About',
      link: 'about.html'
    },
    {
      name: 'Contact',
      link: '#',
      children: [
        {
          name: 'Twitter',
          link: 'https://twitter.com/w_zce'
        },
        {
          name: 'About',
          link: 'https://weibo.com/zceme'
        },
        {
          name: 'divider'
        },
        {
          name: 'About',
          link: 'https://github.com/zce'
        }
      ]
    }
  ],
  pkg: require('./package.json'),
  date: new Date()
}

module.exports = grunt => {
  grunt.initConfig({
    // 删除文件夹
    clean: {
      dist: 'dist/**',
      temp: 'temp/**'
    },
    // 样式编译
    sass: {
      options: {
        implementation: sass
      },
      main: {
        files: [{
          expand: true,
          cwd: 'src/assets/styles',
          src: ['*.scss'],
          dest: 'temp/assets/styles',
          ext: '.css'
        }]
      }
    },
    // js 编译
    babel: {
      options: {
        presets: ['@babel/preset-env']
      },
      main: {
        files: [{
          expand: true,
          cwd: 'src/assets/scripts',
          src: ['*.js'],
          dest: 'temp/assets/scripts'
        }]
      }
    },
    // html 编译
    web_swig: {
      options: {
        swigOptions:{
          cache: false
        },
        getData: () => {
          return data
        }
      },
      main: {
        files: [
          {
            expand: true,
            cwd: 'src/',
            src: ['*.html'],
            dest: 'temp/'
          },
        ]
      }
    },
    // 图片压缩
    imagemin: {
      static: {
        options: {
          optimizationLevel: 3,
          svgoPlugins: [{removeViewBox: false}]
        },
        files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.{png,svg}'],
          dest: 'dist/'
        }]
      }
    },
    // useref 文件引用
    useref: {
      html: 'temp/*.html',
      temp: 'temp'
    },
    // 合并
    concat: {
      main: {
        files: {

        }
      }
    },
    // 压缩 js 文件
    uglify: {
      main: {
        files: [{
          expand: true,
          cwd: 'temp/assets/scripts',
          src: ['*.js'],
          dest: 'dist/assets/scripts'
        }]
      }
    },
    // 压缩 css 文件
    cssmin: {
      main: {
        files: [{
          expand: true,
          cwd: 'temp/assets/styles',
          src: ['*.css'],
          dest: 'dist/assets/styles'
        }]
      }
    },
    // 压缩 html 文件
    htmlmin: {
      options: {
        removeComments: true,       // 去除注释信息
        collapseWhitespace: true,    // 去除空白字符
        removeEmptyAttributes: true,  // 去除标签的空属性
        removeCommentsFromCDATA: true, // 去除 CDATA 的注释信息
        removeRedundantAttributes: true     // 去除标签的冗余属性
      },
      build: {
        files: [{
          expand: true,
          cwd: 'temp',
          src: ['*.html'],
          dest: 'dist'
        }]
      }
    },
    copy: {
      public: {
        expand: true,
        src: 'public/**',
        dest: 'dist/',
      }
    },
    // 监听
    watch: {
      options: {
        livereload: '<%=connect.options.livereload%>'  //监听前面声明的端口  35729
      },
      js: {
        files: ['src/**/*.js'],
        tasks: ['babel']
      },
      css: {
        files: ['src/**/*.scss'],
        tasks: ['sass']
      },
      html: {
        files: ['src/*.html', 'src/**/*.html'],
        tasks: ['web_swig']
      }
    },
    connect: {
      options: {
        port: 9000,
        hostname: '*', //默认就是这个值，可配置为本机某个 IP，localhost 或域名
        livereload: 35729  //声明给 watch 监听的端口
      },

      server: {
        options: {
          open: true,
          base: ['temp', 'src', 'public'],
          routes: {
            '/node_modules': 'node_modules'
          }
        }
      },
      build: {
        options: {
          open: false,
          base: ['dist']
        }
      },
    },
    jshint : {
      options : {
        camelCase : true
      },
      core : {
        src : 'src/assets/scripts/*.js'
      }
    }
  })

  loadGruntTasks(grunt) // 自动加载所有的 grunt 插件中的任务

  grunt.registerTask('compile', ['babel', 'sass', 'web_swig'])
  grunt.registerTask('compress', ['uglify', 'cssmin', 'htmlmin'])

  grunt.registerTask('clear', ['clean'])
  grunt.registerTask('lint', ['jshint'])
  grunt.registerTask('build', ['clear', 'compile', 'useref', 'compress', 'imagemin', 'copy'])
  grunt.registerTask('serve', ['connect:server', 'watch'])
  grunt.registerTask('start', ['build', 'connect:build'])
  grunt.registerTask('deploy', ['compile', 'serve'])
}
