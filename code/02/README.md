# 编程题 - 使用 Gulp 完成项目的自动化构建 - 项目说明文档

1. 下载基础代码

2. 需求分析

   1. 文件类型包括 html、js、scss、字体文件、图片

   2. 自动化构建要完成：读取文件 - 加工文件（样式编译、脚本编译、页面模板编译、图片和字体文件转换、文件压缩） - 写入文件

   3. 开发服务器、监视变化

   4. 需要支持的命令：

      ```
      yarn lint
      yarn compile
      yarn serve
      yarn build
      yarn start
      yarn deploy
      ```

      

3. 修改 gulpfile.js 文件

    1. 安装 gulp，并按需引入

       ```
       yarn add gulp --dev
       ```

       ```javascript
       const { src, dest, parallel, series, watch } = require('gulp')
       ```

    2. 安装并导入自动加载插件

       ```bash
       yarn add gulp-load-plugins --dev
       ```

       ```javascript
       const loadPlugins = require('gulp-load-plugins')
       
       const plugins = loadPlugins()
       ```

    3. 开发服务器、监视变化

       ```
       yarn add browser-sync --dev
       ```

       ```javascript
       const browserSync = require('browser-sync')
       const bs = browserSync.create()
       
       const serve = () => {
         watch('src/assets/styles/*.scss', style)
         watch('src/assets/scripts/*.js', script)
         watch('src/*.html', page)
         // watch('src/assets/images/**', image)
         // watch('src/assets/fonts/**', font)
         // watch('public/**', extra)
         watch([
           'src/assets/images/**',
           'src/assets/fonts/**',
           'public/**'
         ], bs.reload)
       
         bs.init({
           notify: false,
           port: 2080,
           // open: false,
           // files: 'dist/**',
           server: {
             baseDir: ['temp', 'src', 'public'],
             routes: {
               '/node_modules': 'node_modules'
             }
           }
         })
       }
       ```

       

    4. 样式编译

       ```
       yarn add gulp-sass --dev
       ```

       ```javascript
       const style = () => {
         return src('src/assets/styles/*.scss', { base: 'src' })
           .pipe(plugins.sass({ outputStyle: 'expanded' }))
           .pipe(dest('temp'))
           .pipe(bs.reload({ stream: true }))
       }
       ```

    5. 脚本编译

       ```
       yarn add @babel/core @babel/preset --dev
       ```

       ```javascript
       const script = () => {
         return src('src/assets/scripts/*.js', { base: 'src' })
           .pipe(plugins.babel({ presets: ['@babel/preset-env'] }))
           .pipe(dest('temp'))
           .pipe(bs.reload({ stream: true }))
       }
       ```

    6. 页面模板编译

       ```
       yarn add gulp-swig --dev
       ```

       ```javascript
       const page = () => {
         return src('src/*.html', { base: 'src' })
           .pipe(plugins.swig({ data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
           .pipe(dest('temp'))
           .pipe(bs.reload({ stream: true }))
       }
       ```

    7. 图片和字体文件转换

       ```
       yarn add gulp-imagemin --dev
       ```

       ```javascript
       const image = () => {
         return src('src/assets/images/**', { base: 'src' })
           .pipe(plugins.imagemin())
           .pipe(dest('dist'))
       }
       
       const font = () => {
         return src('src/assets/fonts/**', { base: 'src' })
           .pipe(plugins.imagemin())
           .pipe(dest('dist'))
       }
       ```

    8. 其他文件写入

       ```javascript
       const extra = () => {
         return src('public/**', { base: 'public' })
           .pipe(dest('dist'))
       }
       ```

    9. 文件清除

       ```
       yarn add del --dev
       ```

       ```
       const del = require('del')
       
       const clean = () => {
         return del(['dist', 'temp'])
       }
       ```

    10. useref 文件引用及文件压缩处理

        安装 useref 插件

        压缩 js 文件插件： gulp-clean-css

        压缩 css 文件插件：gulp-uglify

        压缩 html 文件插件：gulp-htmlmin

        使用 if 判断需要用到 gulp-if 插件

        ```
        yarn add gulp-useref --dev
        yarn add gulp-htmlmin gulp-uglify gulp-clean-css --dev
        yarn add gulp-if --dev
        ```

        ```javascript
        const useref = () => {
          return src('temp/*.html', { base: 'temp' })
            .pipe(plugins.useref({ searchPath: ['temp', '.'] }))
            // 压缩 html js css 文件
            .pipe(plugins.if(/\.js$/, plugins.uglify()))
            .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
            .pipe(plugins.if(/\.html$/, plugins.htmlmin({
              collapseWhitespace: true,
              minifyCSS: true,
              minifyJS: true
            })))
            .pipe(dest('dist'))
        }
        ```

    11. eslint 校验

        ```bash
        yarn add gulp-eslint --dev
        ```

        ```javascript
        const lint = () => {
          return src('src/**')
            .pipe(plugins.eslint({
              rules: {
                'camelcase': 1,
                'quotes': 0
              }
            }))
            .pipe(plugins.eslint.format())
        }
        ```

    12. 运行打包后的文件

        ```javascript
        const buildServe = () => {
          bs.init({
            server: {
              baseDir: 'dist',
              index : "/index.html"  // 将dist目录下的index.html作为入口页面访问
            }
          })
        }
        ```

    13. 规划构建任务

        ```javascript
        const compile = parallel(style, script, page)
        
        const build =  series(
          clean,
          parallel(
            series(compile, useref),
            image,
            font,
            extra
          )
        )
        
        const start = series(
          build,
          buildServe
        )
        
        const deploy = series(compile, serve)
        
        module.exports = {
          clean,
          lint,
          build,
          serve,
          start,
          deploy
        }
        ```

4. 运行命令测试

