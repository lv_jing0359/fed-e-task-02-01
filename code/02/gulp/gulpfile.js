// 实现这个项目的构建任务

// 按需引入 gulp
const { src, dest, parallel, series, watch } = require('gulp')

// 自动加载插件
const loadPlugins = require('gulp-load-plugins')

const plugins = loadPlugins()

// 开发服务器、监视变化
const browserSync = require('browser-sync')
const bs = browserSync.create()

// 文件清除
const del = require('del')

const clean = () => {
  return del(['dist', 'temp'])
}

// 样式编译
const style = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

// 脚本编译
const script = () => {
  return src('src/assets/scripts/*.js', { base: 'src' })
    .pipe(plugins.babel({ presets: ['@babel/preset-env'] }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

// 页面模板编译
const data = {
  menus: [
    {
      name: 'Home',
      icon: 'aperture',
      link: 'index.html'
    },
    {
      name: 'About',
      link: 'about.html'
    },
    {
      name: 'Contact',
      link: '#',
      children: [
        {
          name: 'Twitter',
          link: 'https://twitter.com/w_zce'
        },
        {
          name: 'About',
          link: 'https://weibo.com/zceme'
        },
        {
          name: 'divider'
        },
        {
          name: 'About',
          link: 'https://github.com/zce'
        }
      ]
    }
  ],
  pkg: require('./package.json'),
  date: new Date()
}

const page = () => {
  return src('src/*.html', { base: 'src' })
    .pipe(plugins.swig({ data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

// 图片和字体文件转换
const image = () => {
  return src('src/assets/images/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

const font = () => {
  return src('src/assets/fonts/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

// 其他文件写入
const extra = () => {
  return src('public/**', { base: 'public' })
    .pipe(dest('dist'))
}

// useref 文件引用及文件压缩处理
const useref = () => {
  return src('temp/*.html', { base: 'temp' })
    .pipe(plugins.useref({ searchPath: ['temp', '.'] }))
    // 压缩 html js css 文件
    // .pipe(plugins.if(/\.js$/, plugins.uglify()))
    // .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
    // .pipe(plugins.if(/\.html$/, plugins.htmlmin({
    //   collapseWhitespace: true,
    //   minifyCSS: true,
    //   minifyJS: true
    // })))
    .pipe(dest('dist'))
}

// 服务器
const serve = () => {
  watch('src/assets/styles/*.scss', style)
  watch('src/assets/scripts/*.js', script)
  watch('src/*.html', page)
  // watch('src/assets/images/**', image)
  // watch('src/assets/fonts/**', font)
  // watch('public/**', extra)
  watch([
    'src/assets/images/**',
    'src/assets/fonts/**',
    'public/**'
  ], bs.reload)

  bs.init({
    notify: false,
    port: 2080,
    // open: false,
    // files: 'dist/**',
    server: {
      baseDir: ['temp', 'src', 'public'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}

// eslint 校验
const lint = () => {
  return src('src/**')
    .pipe(plugins.eslint({
      rules: {
        'camelcase': 1,
        'quotes': 0
      }
    }))
    .pipe(plugins.eslint.format())
}

const compile = parallel(style, script, page)

// 运行打包后的文件
const buildServe = () => {
  bs.init({
    server: {
      baseDir: 'dist',
      index : "/index.html"  // 将dist目录下的index.html作为入口页面访问
    }
  })
}

const build =  series(
  clean,
  parallel(
    series(compile, useref),
    image,
    font,
    extra
  )
)

const start = series(
  build,
  buildServe
)

const deploy = series(compile, serve)

module.exports = {
  clean,
  lint,
  build,
  serve,
  start,
  deploy
}
